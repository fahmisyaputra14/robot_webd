***Settings***
Library     SeleniumLibrary 
Resource    ../../resources/Setup.robot
Resource    video_lineupKeywords.robot

Test Setup      Open Browser    ${siteUrl}
Test Teardown   Close my Browser

***Variables***
${siteUrl}  https://www.rctiplus.com/

***Test Cases***
Play Video Line Up "Ikatan Cinta" Episode
    Given user in "Ikatan Cinta" Episode
    When user play one of episode from the list
    And user click play/pause player
    Then user validate the video

Play Video Line Up "Ikatan Cinta" Extras
    Given user in "Ikatan Cinta" Extras
    When user play one of extras from the list
    and user click play/pause player
    Then user validate the video

Play Video Line Up "Ikatan Cinta" Clips
    Given user in "Ikatan Cinta" Clips
    When user play one of clips from the list
    and user click play/pause player
    Then user validate the video