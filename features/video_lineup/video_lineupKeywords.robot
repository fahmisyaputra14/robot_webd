***Settings***
Library     SeleniumLibrary

***Variables***
${IkatanCinta}  xpath://*[@id="r-trending-ctn"]/div[1]/div/div/ul[1]/li/a/h5
${Episode}      id:episode
${videoepisode}     xpath://*[@id="program-episodes-ctn"]/div[1]/div/div/ul[1]/li/a/img
${player}       xpath://*[@id="vid-player"]/div[2]/div[4]/video
${playpause}    xpath://*[@id="vid-player"]/div[2]/div[12]/div[1]/div/div/div[2]/div
${fastforward}  xpath://*[@id="play10sekforward"]/button
${previous}     xpath://*[@id="vid-player"]/div[2]/div[12]/div[1]/div/div/div[1]/div
${Extra}        id:extra
${videoextra}   xpath://*[@id="program-extras-ctn"]/div[1]/div/div/ul[2]/li/a/h7
${Clips}        id:clip
${videoclips}   xpath://*[@id="program-clips-ctn"]/div[1]/div/div/ul[1]/li/a/img

# ${playpauseExtra}   xpath://*[@id="vid-player"]/div[2]/div[12]/div[1]/div/div/div[2]/div



***Keywords***
user in "Ikatan Cinta" Episode
    click element   ${IkatanCinta}
    sleep   10 seconds
    click element   ${Episode}
    sleep   10 seconds

user in "Ikatan Cinta" Extras
    click element   ${IkatanCinta}
    sleep   10 seconds
    click element   ${Extra}
    sleep   10 seconds

user in "Ikatan Cinta" Clips
    click element   ${IkatanCinta}
    sleep   10 seconds
    click element   ${Clips}
    sleep   20 seconds

user play one of clips from the list
    sleep   5 seconds
    click element   ${videoclips}
    wait until element is visible   ${player}

user play one of episode from the list
    # wait until element is visible   ${videoepisode}    10 seconds
    sleep   5 seconds
    click element   ${videoepisode}
    wait until element is visible   ${player}

user play one of extras from the list
    sleep   5 seconds
    click element   ${videoextra}
    wait until element is visible   ${player}

user click play/pause player
    # set selenium speed  0.5 second
    wait until element is visible       ${playpause}    20 seconds
    click element   ${playpause}
    sleep   60 seconds
    # wait until element is visible   ${player}   10 seconds
    click element   ${player}
    click element   ${playpause}

user validate the video
    page should contain element     ${playpause}


# user click banner "Ikatan Cinta" Episode
#     # Scroll Element Into View    id:r-trending-ctn
#     click element   ${IkatanCinta}
#     sleep   10 seconds
#     click element   ${Episode}
#     sleep   10 seconds

# user plays one of episode from the list
#     # wait until element is visible   ${videoepisode}    10 seconds
#     sleep   5 seconds
#     click element   ${videoepisode}

# user click play/pause player
#     # set selenium speed  0.5 second
#     wait until element is visible       ${playpause}    20 seconds
#     click element   ${playpause}
#     sleep   60 seconds
#     wait until element is visible   ${player}   20 seconds
#     click element   ${playpause}

# user validate the video
#     page should contain element     ${playpause}

# user click banner "Ikatan Cinta" Extras
#     click element   ${IkatanCinta}
#     sleep   10 seconds
#     click element   ${Extra}
#     sleep   10 seconds

# user play one of extras from the list
#     sleep   5 seconds
#     click element   ${videoextra}

# # user click play/pause player
# #     wait until element is visible       ${playpause}    20 seconds
# #     click element   ${playpause}
# #     sleep   60 seconds
# #     wait until element is visible   ${player}   20 seconds
# #     click element   ${playpause}

# # user validate the extras
# #     page should contain element     ${playpause}