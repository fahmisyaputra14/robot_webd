***Settings***
Library     SeleniumLibrary

***Variables***
${mailField}     id:username
${passField}    id:password
${login_btn}    id:btnLogin
${invalidusermodal}     xpath://*[@id="swal2-content"]

***Keywords***
Enter user name
    [Arguments]     ${userName}
    wait until element is visible      ${mailField}
    input Text      ${mailField}    ${userName}

Enter Password
    [Arguments]     ${password}
    input text      ${passField}    ${password}

Click Login Button
    click button    ${login_btn}

Verify Login Successfull
    wait until page contains        RCTI+ - Satu Aplikasi, Semua Hiburan        10 seconds
    title should be     RCTI+ - Satu Aplikasi, Semua Hiburan

Verify email is incorrect
    wait until element is visible   ${invalidusermodal}     10 seconds
    page should contain element     ${invalidusermodal}     Invalid, User Has Not Been Registered