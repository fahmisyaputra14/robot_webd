***Settings***
Library     SeleniumLibrary
Resource    LoginKewords.robot
Resource    ../../resources/Setup.robot

Test Setup      Open Browser    ${siteUrl}
Test Teardown   Close my Browser

***Variables***
${siteUrl}              https://www.rctiplus.com/login
${userName}             fahmisyaputra14@yahoo.com
${password}             14juni1994
${incorrectmail}        fahmisyaputra@yahoo.com
${incorrectpassword}    1234567890


***Test Cases***
Login with valid email
    Enter user name     ${userName}
    Enter password      ${password}
    Click Login Button
    verify login successfull

Login with incorrect email
    Enter user name     ${incorrectmail}
    Enter password      ${password}
    Click Login Button
    verify email is incorrect